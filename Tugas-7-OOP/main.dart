import 'Segitiga.dart';

void main() {
  Segitiga sisi;
  double luassisi;

  sisi = new Segitiga();
  sisi.setengah = 0.5;
  sisi.alas = 20.0;
  sisi.tinggi = 30.0;

  luassisi = sisi.hitungluas();
  print(luassisi);
}
