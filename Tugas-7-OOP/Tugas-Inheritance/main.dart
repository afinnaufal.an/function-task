import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  armor_titan ar = armor_titan();
  attact_titan at = attact_titan();
  beast_titan bt = beast_titan();
  human hm = human();

  ar.powerPoint = 70;
  at.powerPoint = 75;
  bt.powerPoint = 80;
  hm.powerPoint = 85;

  print("power point armor titan: ${ar.powerPoint}");
  print("power point attack titan: ${at.powerPoint}");
  print("power point beast titan: ${bt.powerPoint}");
  print("power point human: ${hm.powerPoint}");
}
