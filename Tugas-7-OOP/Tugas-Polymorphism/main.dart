import 'lingkaran2.dart';
import 'bangun_datar.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  bangun_datar bd = bangun_datar();
  segitiga sg = segitiga(20, 15, 25);
  lingkaran lg = lingkaran(7);
  persegi ps = persegi(10);

  bd.luas();

  print("luas segitiga = ${sg.luas()}");
  print("luas lingkaran = ${lg.luas()}");
  print("luas persegi = ${ps.luas()}");

  bd.keliling();

  print("keliling segitiga = ${sg.keliling()}");
  print("keliling lingkaran = ${lg.keliling()}");
  print("keliling persegi = ${ps.keliling()}");
}
