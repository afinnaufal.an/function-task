import 'dart:async';
import 'Tugas-Inheritance/human.dart';

void main() {
  var h = human();

  print("Luffy");
  print("Zoro");
  print("Killer");

  var timer = Timer(
      Duration(seconds: 3), () => h.getData().then((value) => print(h.name)));
}

class human {
  String name = "name character one piece";

  Future<void> getData() async {
    name = "Hilmy";
    print("get data [done]");
  }
}
